const Url = require('url');
const _ = require('highland');

// passing in all dependencies can make functions easier to test
const getDomainName = _.curry((url, uri) => {
  if (url.parse(uri).hostname) {
    return url.parse(uri).hostname.replace('www.', '');
  } else {
    return "UNKNOWN";
  }
});

module.exports.getScrapeUrls = req => {
  return req.body.siteRoots.trim().split(',').map(uri => uri.trim());
};

module.exports.getSearchText = req => {
  return req.body.searchText.trim();
};

module.exports.getDomainName = getDomainName(Url);
