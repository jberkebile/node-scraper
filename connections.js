const yaml = require('js-yaml');
const existsSync = require('fs').existsSync;
const readFileSync = require('fs').readFileSync;

function readConfig() {
  try {
    const confFile = ['./dev.yaml', './docker.yaml', './prod.yaml'].filter(existsSync)[0];
    return yaml.safeLoad(readFileSync(confFile, 'utf8'));
  } catch (err) {
    console.error('There was an error reading the config file for the environment');
    console.error(err);
    return null;
  }
}

const config = readConfig();

const connectRabbitMQ = conf => require('amqplib').connect({
  protocol: 'amqp',
  hostname: conf.rabbitmq,
  port: 5672
});

const connectRedis = conf => {
  return require('handy-redis').createHandyClient({host: conf.redis, port: 6379});
};

const connectElasticSearch = conf => {
  return new require('elasticsearch').Client({
    host: `${conf.elasticsearch}:9200`,
    log: 'warning'
  });
};

module.exports.connectRabbitMQ = connectRabbitMQ(config);
module.exports.connectRedis = connectRedis(config);
module.exports.connectElasticSearch = connectElasticSearch(config);
