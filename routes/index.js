const express = require('express');
const router = express.Router();
const puppeteer = require('puppeteer');

// Local imports
const scraperUtil = require('../util/util.js');
const searchCrawler = require('../crawlers/searchEngineCrawler.js');
const elasticSink = require('../sinks/elasticSearchSink.js');
const connectRabbitMq = require('../connections.js').connectRabbitMQ;

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'The Simplest Web Crawler' });
});

// This post should be refactored out into a curried function so
// that it can take in the AMQP connection as a dependency at server startup
// The would eliminate the need to grab the connection from multiple locations
// in this app.
router.post('/', async (req, res, next) => {
  const urlsToScrape = scraperUtil.getScrapeUrls(req);
  const domainNames = urlsToScrape.map(scraperUtil.getDomainName);
  const urlDomainPairs = urlsToScrape.map((u, i) => [u, domainNames[i]]);
  const searchText = scraperUtil.getSearchText(req);

  try {
    const rabbitClient = await connectRabbitMq;
    const ch = await rabbitClient.createChannel();
    await Promise.all(
      urlDomainPairs.map(
        pair => ch.sendToQueue('crawl-urls', new Buffer(pair.concat([searchText]).join(',')))
      )
    );
    ch.close();
  } catch (err) {
    console.error(err);
  }

  res.redirect('/');
});

module.exports = router;
