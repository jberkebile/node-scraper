const _ = require('highland');

/*******************************************************************************
 * This module requires a Pupeteer Browser
 *
 * The browser isn't cached because each call should get its own browser.  It
 * seemed overly-complicated getting this module to automatically cache a
 * browser into the curried function before passing the value back from the
 * module.  For a PoC, this is fine, but the best thing to do would be to
 * swing back later and make this a Factory Class.
 ******************************************************************************/
module.exports.crawlPage = _.curry(async (uri, index, searchText, browser) => {
  // I think the function was returning before it could close the browser
  var returnVal = [null, index];
  try {
    const page = await browser.newPage();
    console.info(`GOING TO: ${uri}`);
    await page.goto(uri);
    await page.waitFor(1000);
    await page.keyboard.type(searchText);
    await page.keyboard.press('Enter');
    await page.waitFor(1000);
    const html = await page.evaluate(() => document.body.innerHTML);
    browser.close();
    returnVal = [html, index];
  } catch (err) {
    console.error(`An exception was encountered while crawling domain ${index}\n${err}`);
  } finally {
    browser.close();
  }
  return returnVal;
});

module.exports.crawlListener = amqpClient => puppeteer => async (urlQueue, crawlFxn, destQueue) => {
  // This code is reusable and should be refactored out into a common function
  var ch = null;
  try {
    ch = await amqpClient.createChannel(urlQueue, {durable: false});
    await ch.assertQueue(urlQueue, {durable: false});
  } catch (err) {
    console.error(`SCRAPER: ${err}`);
    return;
  }
  process.on('exit', () => {
    ch.close();
  });

  // Create 9 listeners assuming 8 CPU cores, normally CPUs + 1 is a good place to start for parallelism.
  for (var i = 0; i < 2; i++) {
    ch.consume(urlQueue, msg => {
      const [uri, domain, searchText] = msg.content.toString().split(',');
      console.info(`SPLIT: ${uri} ${domain} ${searchText}`);

      puppeteer.launch().then(
        browser => crawlFxn(uri, domain, searchText, browser).then(
          ([html, index]) => {
            if (html) {
              ch.assertQueue(destQueue, {durable: false})
                .then(ok => ch.sendToQueue(destQueue, new Buffer([index, html].join(','))));
            } else {
              console.warn(`No HTML was returned from a page on domain ${index}`);
            }
        }).catch(console.error)
      ).catch(console.error);
    }, {noAck: true});
  }
};
