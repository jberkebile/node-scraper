const _ = require('highland');
const elasticSearchClient = require('../connections.js').connectElasticSearch;

process.on('exit', () => {
  elasticSearchClient.close();
});

// When this becomes concurrent, the edge case that stops the recursive spidering
// will be when the domain changes
const saveTextToElasticSearch = _.curry(async (elasticSearchClient, html, index) => {
  var indexExists = null;
  try {
    indexExists = await elasticSearchClient.indices.exists({ index: index });
  } catch (err) {
    console.error(`Could not save text to ElasticSearch\n${err}`);
  }

  console.info(`Index ${index} Exists? ${indexExists}`);
  if (!indexExists) {
    const indexCreateRes = await elasticSearchClient.indices.create({
      index: index
    });
    console.log(`index create response = ${JSON.stringify(indexCreateRes)}`);
  }
  const saveRes = await elasticSearchClient.index({
    index: index,
    type: 'search results',
    body: { 'html': html }
  });
  console.info(`Response from trying to index scraped HTML: ${JSON.stringify(saveRes)}`);
});

module.exports.exportText = saveTextToElasticSearch(elasticSearchClient);

module.exports.exportListener = amqpClient => async (sourceQueue, sinkFxn) => {
  // This code is reusable and should be refactored out into a common function
  var ch = null;
  try {
    ch = await amqpClient.createChannel(sourceQueue, {durable: false});
    await ch.assertQueue(sourceQueue, {durable: false});
  } catch (err) {
    console.error(`ExportListener: ${err}`);
    return; // exit early if the channel cannot be created
  }
  process.on('exit', () => {
    ch.close();
  });

  // create 18 consumers, this function is lighter weight, so we should be able
  // to handle more simultaneous listeners.
  for (var i = 0; i < 6; i++) {
    ch.consume(sourceQueue, msg => {
      const [index, html] = msg.content.toString().split(/,(.+)/);
      console.info(`SINK: htmlLen: ${html.length} domain: ${index}`);
      if (html) { sinkFxn(html, index); }
      else {
        console.warn(`The export content for a page in domain ${index} was ${html}`);
      }
    }, {noAck: true});
  }
};
