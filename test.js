import test from 'ava';
const app = require('./app.js');
const connections = require('./connections.js');

test('redis connection', async t => {
  const redisClient = connections.connectRedis('localhost', 6379);
  await redisClient.del("hello");
  const result = await redisClient.zadd("hello", 1, "hello, world!");
  const hello = await redisClient.zrank("hello", "hello, world!");
  const notPresent = await redisClient.zrank("hello", "not present");
  t.is(hello, 0);
  t.is(notPresent, null);
});

test.cb('ElasticSearch connection', t => {
  const elasticSearchClient = connections.connectElasticSearch('localhost', 9200);
  t.plan(1);
  t.log("hello");
  elasticSearchClient.ping({
    requestTimeout: 1000
  }).then((res, err) => {
    if (err) {
      console.error(err);
      console.error('Could NOT connect to ElasticSearch');
      t.fail();
    } else {
      t.pass();
    }
    t.end();
  });
});

// Testing a callback inside of an async function is not supported, so I
// used the Promise API instead.
test.cb('RabbitMQ connection', t => {
  t.plan(1);
  const rabbitClient = connections.connectRabbitMQ;
  rabbitClient
    .then(conn => conn.createChannel())
    .then(ch => {
      ch.assertQueue('hello', {durable: false})
        .then(ok => ch.sendToQueue('hello', new Buffer('Hello, world!')));
    }).catch(console.error);

  rabbitClient
    .then(conn => conn.createChannel())
    .then(ch => {
      ch.assertQueue('hello', {durable: false})
        .then(ok => ch.consume('hello', msg => {
          t.log(msg.content.toString());
          if (msg.content.toString() == 'Hello, world!') { t.pass(); }
          else { t.fail(); }
          t.end();
        }));
    }).catch(console.error);
});
