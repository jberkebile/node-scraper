const scraperUtil = require('../util/util.js');

module.exports.scrape = async (html, index, browser) => {
  // I think the function was returning before it could close the browser
  var returnVal = null;
  try {
    if (html) {
      console.info(`LENGTH: ${html.length}`);
    }
    const page = await browser.newPage();
    await page.goto(`data:text/html,${html}`, { waitUntil: 'networkidle0' });
    await page.waitFor(1000);
    const content = await page.evaluate(() => document.body.innerHTML);
    const links = await page.evaluate(
      () => Array.from(document.querySelectorAll('a')).map((val) => val.href)
    );
    returnVal = links;
  } catch (err) {
    console.error(`An exception was encountered while scraping domain ${index}\n${err}`);
  } finally {
    browser.close();
  }
  return returnVal;
};

// This function should be divided up into multiple smaller functions.  It's too big.
module.exports.scrapeListener = amqpClient => puppeteer => async (sourceQueue, scrapeFxn, crawlQueue, sinkQueue) => {
  // This code is reusable and should be refactored out into a common function
  var ch = null;
  try {
    console.info(sourceQueue);
    ch = await amqpClient.createChannel(sourceQueue, {durable: false});
    await ch.assertQueue(sourceQueue, {durable: false});
  } catch (err) {
    console.error(`SCRAPER: ${err}`);
    if (ch)
    return;
  }
  process.on('exit', () => {
    ch.close();
  });

  // This is a slightly heavier function, so I'll only create 9 consumers
  for (var i = 0; i < 2; i++) {
    ch.consume(sourceQueue, msg => {
      const [index, html] = msg.content.toString().split(/,(.+)/);
      if (html) {
        // Forward the links to be crawled
        ch.assertQueue(crawlQueue, {durable: false}).then(
          ok => puppeteer.launch().then(
            browser => scrapeFxn(html, index, browser).then(links => {
              if (links) {
                const cleanLinks = links.filter(l => !!l);
                const domainNames = links.map(scraperUtil.getDomainName);
                const linkDomainPairs = links.map((u, i) => [u, domainNames[i]]);

                linkDomainPairs.forEach(
                  pair => {
                    if (pair[0].trim().length > 0 && pair[1].trim().length > 0) {
                      console.info(`SENDING: ${typeof pair.concat(['abc']).join(',')} to be crawled`);
                      ch.sendToQueue(crawlQueue, new Buffer(pair.concat(['abc']).join(',')));
                    }
                  }
                );
              } else {
                console.info(`Skipping a page on domain ${index} - no links were found`);
              }
            }).catch(console.error)
          ).catch(console.error)
        ).catch(console.error);

        // Forward the HTML to be stored in ElasticSearch
        ch.assertQueue(sinkQueue, {durable: false})
          .then(ok => ch.sendToQueue(sinkQueue, new Buffer(msg.content.toString())))
          .catch(console.error);
      } else {
        console.warn(`Skipping scraping for a link in domain ${index}, the HTML content could not be gathered`);
      }
    }, {noAck: true});
  }
};
