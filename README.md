# Overview

A simple NodeJS webscraper prototype.

# Development

## General Tools

1. You'll need Nix to set-up the NodeJS development sandbox.
    - Get Nix [Nix Package Manager Download with Instructions](https://nixos.org/nix/download.html)
        - `curl https://nixos.org/nix/install | sh`
2. You'll need Docker in order to run ElasticSearch and Redis for intergration testing during development.  Docker is also used to package and deploy the application.  Docker cannot be installed through Nix AFAIK because the installation is different between OS X and Linux.
    - Install Docker Engine
        - [Mac/OSX](https://docs.docker.com/docker-for-mac/install/)
        - [Windows](https://docs.docker.com/docker-for-windows/install/)
        - [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
        - [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
        - [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - If you are on Linux and want to execute Docker without `sudo`
        1. `sudo groupadd docker`
        2. `sudo gpasswd -a $USER docker`
        3. `newgrp docker`
        4. Test that docker works without sudo
            1. `nix-shell`
            2. `docker run hello-world`

## Setting-up the Development Environment

1. Run the following commands in sequence in this project's root directory.
    - `nix-shell # creates a development sandbox BASH shell with all dependencies installed.`
        - This may take a while the first time.
    - `npm i     # installs the NPM dependencies for the project`
    - If you use Emacs/Spacemacs, you can get access to the shell sandbox evironment by running Emacs as a headless server and connecting to it from you Emacs GUI client.
        - `emacs --daemon`
        - `emacsclient -t &`
    - Vim also has a client-server mode that lets you do the same thing as above.
        - https://www.rohanjain.in/yet-another-vim-productivity-post-server-client/
2. This project depends on the ElasticSearch Docker Container.
    1. [Getting the container](https://www.docker.elastic.co/)
        - `docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.3`
    2. [Running the container](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
        - `docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.2.3` 
        - Test the ports
            - `nc -z -v -w5 localhost 9200`
            - `nc -z -v -w5 localhost 9300`
3. This project also relies on the RabbitMQ Docker Container to provide a resilient cache of URLs to visit
    1. [Getting the Container](https://hub.docker.com/_/rabbitmq/)
        - `docker pull rabbitmq:3.7.4-alpine`
        - Alpine was chosen because it **should** provide a lighter-weight container.
    2. [Running the Container](https://docs.docker.com/samples/library/rabbitmq/)
        - `docker run -p 5672:5672 --name rabbitmq -d rabbitmq:3.7.4-alpine`
        - Test the ports
            - `nc -z -v -w5 localhost 6379`
4. This project also relies on the Redis Docker Container to provide a resilient cache of visited URLs
    1. [Getting the Container](https://store.docker.com/images/redis)
        - `docker pull redis:4.0.8-alpine`
        - Alpine was chosen because it **should** provide a lighter-weight container.
    2. [Running the Container](https://docs.docker.com/samples/library/redis/)
        - `docker run -p 6379:6379 --name redis -d redis:4.0.8-alpine`
        - Test the ports
            - `nc -z -v -w5 localhost 6379`

## Working in the Development Environment

- `nix-shell` gets you into the development sandbox.
- Nothing is installed globally to your OS environment.
    - Any NPM packages that serve as executables, such as express-generator or the AVA test runner commands should be executed via `npx`.
    - Any system-level dependencies should be declared in `default.nix`, which defines the `nix-shell` development sandbox environment.
- Run the ExpressJS web server
    - `DEBUG=scraper:* npm start` 
- Run the unit tests
    - `npm test`
- Run the Docker Containers
    - ElasticSearch
        - `docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.2.3` 
        - Test the ports
            - `nc -z -v -w5 localhost 9200`
            - `nc -z -v -w5 localhost 9300`
    - RabbitMQ
        - `docker run -d -p 5672:5672 rabbitmq:3.7.4-alpine`
        - Test the ports
            - `nc -z -v -w5 localhost 5672`
    - Redis
        - `docker run -p 6379:6379 --name redis -d redis:4.0.8-alpine`
        - Test the ports
            - `nc -z -v -w5 localhost 6379`
- Querying for stored ElasticSearch documents
    - localhost:9200/<index to query>/_search?q=<query string>
    - https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html
- Git-Flow was used to define the development process on this project.  Please follow the below standard:
    - `master` branch is for production releases
    - `develop` branch is the unstable development branch
    - Feature branches start with `feature/`
    - Bigfix branches start with `bugfix/`
    - Release branches start with `release/`
    - Hotfix branches start with `hotfix/`
    - Support branches start with `support/`

## Local Deployment

You can build this code into a Docker Container for deployment to an environment via the following command.

- `docker build -t josiah14/node-scraper:latest .`

Once that's done, the plan is to stand-up the entire system through Docker Compose.  The Compose script isn't fully functional, yet, however.  When it is, you'll be able to run the entire system with a single command:

 - `docker-compose up`
 
 Docker Compose is included in the Nix development sandbox shell, for you.

## Documentation

- Architectural Diagram
    - https://bitbucket.org/jberkebile/node-scraper/wiki/Home
